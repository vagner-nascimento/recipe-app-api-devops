FROM python:3.7-alpine
LABEL maintainer="London App Developer Ltd"

ENV PYTHONUNBUFFERED 1

# Add scripts folder to the linux path to execute entrypoint sh without write its location
ENV PATH="/scripts:${PATH}"

# Updates pip to the lastest avaliable version to use below
RUN pip install --upgrade pip

COPY ./requirements.txt /requirements.txt
RUN apk add --update --no-cache postgresql-client jpeg-dev
RUN apk add --update --no-cache --virtual .tmp-build-deps \
      gcc libc-dev linux-headers postgresql-dev musl-dev zlib zlib-dev
RUN pip install -r /requirements.txt
RUN apk del .tmp-build-deps

RUN mkdir /app
WORKDIR /app
COPY ./app /app

# Copy local script files to the docker
COPY ./scripts /scripts
# Make all script into the folder executable
RUN chmod +x /scripts/*

RUN mkdir -p /vol/web/media
RUN mkdir -p /vol/web/static
RUN adduser -D user
RUN chown -R user:user /vol/
RUN chmod -R 755 /vol/web
USER user

# change to run over AWS Fargate deployng system
VOLUME /vol/web

# The name of file is egnoth because it has been previously added to the PATH on the line 7
CMD ["entrypoint.sh"]

# Add it to avoid error on AWS Cloud Watch
VOLUME /vol/web