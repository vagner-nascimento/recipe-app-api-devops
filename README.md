# Recipe App API DevOps Starting Point

Source code for my Udemy course Build a [Backend REST API with Python & Django - Advanced](http://udemy.com/django-python-advanced/).

The course teaches how to build a fully functioning REST API using:

 - Python
 - Django / Django-REST-Framework
 - Docker / Docker-Compose
 - Test Driven Development

## Getting started

To start project, run:

```
docker-compose up
```

The API will then be available at http://127.0.0.1:8000

# Terraform website
https://www.terraform.io/

## Terraform AWS EC2 docs
https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance

# NOTES

## TERRAFORM Commands
* Fmt (FOMA): formatar os arquivos tf: 
    * **docker-compose -f deploy/docker-compose.yml run --rm terraform fmt**  // --rm significa para NAO armazenar a imagem

* Validate: valida se os recursos estao corretamente descritos e disponíveis: 
    * **docker-compose -f deploy/docker-compose.yml run --rm terraform validate**

* Plan: cria um plano de execução para subir os recursos do terraform, lista oq sera alterado
    * **docker-compose -f deploy/docker-compose.yml run --rm terraform plan**

* Apply: aplica a criação/atualização de recursos
    * **docker-compose -f deploy/docker-compose.yml run --rm terraform apply**

* Destroy: destroi todos recursos
    * **docker-compose -f deploy/docker-compose.yml run --rm terraform destroy**

## Bastion Commands to create Django Superuser
$(aws ecr get-login --no-include-email --region us-east-1)

docker run -it \
    -e DB_HOST=raad-staging-db.c0auedzbkhzh.us-east-1.rds.amazonaws.com \
    -e DB_NAME=recipe \
    -e DB_USER=recipeapp \
    -e DB_PASS=amBvsI+89 \
    417662913057.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest \
    sh -c "python manage.py wait_for_db && python manage.py createsuperuser"
