# get AMI (Amazon Michine Image)
# data is used ONLYT to get information about aws resources
# aws_ami is the aws data resource name
# amazon_linux is name thath we gave to this data search
data "aws_ami" "amazon_linux" {
  most_recent = true
  # filter to find the desired AMI to use in our EC2 instance
  filter {
    # filter by "name" attribute
    name = "name"
    # name of desired AMI - Get it from EC2 console
    values = ["amzn2-ami-kernel-5.10-hvm-2.0.*-x86_64-gp2"]
  }
  # owner of ami, in this case only from offical amazon ami
  owners = ["amazon"]
}

# Allow bastion to assume a role
resource "aws_iam_role" "bastion" {
  name               = "${local.prefix}-bastion"
  assume_role_policy = file("./templates/bastion/instance-profile-policy.json")

  tags = local.common_tags
}

# Attach the aws policy to read and pull images from ECR
resource "aws_iam_role_policy_attachment" "bastion_attach_policy" {
  role       = aws_iam_role.bastion.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

# Creates an instance profile with the role to attach on the bastion instance
resource "aws_iam_instance_profile" "bastion" {
  name = "${local.prefix}-bastion-instance-profile"
  role = aws_iam_role.bastion.name
}


# set EC2 configurations
# resource is used to effectively create/update the cloud resources
# "bastion" is the name that we gave to this aws resource
# this server is used only for administrative rotines
resource "aws_instance" "bastion" {
  # reffer to the result of data configured above to get the id of seeked AMI
  ami = data.aws_ami.amazon_linux.id
  # the resources that will be used by EC2 instance (network, memory size, etc)
  instance_type        = "t2.micro"
  user_data            = file("./templates/bastion/user-data.sh") # script to run on startup
  iam_instance_profile = aws_iam_instance_profile.bastion.name    # attach profile that allows to push images from ECR
  subnet_id            = aws_subnet.public_a.id
  key_name             = var.bastion_key_name
  vpc_security_group_ids = [
    aws_security_group.bastion.id
  ]

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-bastion" })
  )
}

resource "aws_security_group" "bastion" {
  description = "Control bastion inbound and outbound access"
  name        = "${local.prefix}-bastion"
  vpc_id      = aws_vpc.main.id

  # inbound ssh - to allows to access through external console
  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"] # in production is recomended limitate to only allowed specif IPs - we will allow all ips because we don't has static ip
  }

  # outbound https - to allow download from internet
  egress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound http
  egress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound postgress
  egress {
    from_port = 5432
    to_port   = 5432
    protocol  = "tcp"
    cidr_blocks = [ # can access only the blocks of our private subnets
      aws_subnet.private_a.cidr_block,
      aws_subnet.private_b.cidr_block,
    ]
  }

  tags = local.common_tags
}
