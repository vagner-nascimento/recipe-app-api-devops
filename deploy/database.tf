# RDS subnet to make possible the access to the database only through the private subnets
resource "aws_db_subnet_group" "main" {
  name = "${local.prefix}-main"
  subnet_ids = [
    aws_subnet.private_a.id,
    aws_subnet.private_b.id
  ]

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-main" })
  )
}

# Control the inbound and outbound access to the resource
resource "aws_security_group" "rds" {
  description = "Allow access to the RDS database instance."
  name        = "${local.prefix}-rds-inbound-access"
  vpc_id      = aws_vpc.main.id

  # ingress means the rules to inboubd access / egress are rules to outbound access
  ingress {
    protocol  = "tcp"
    from_port = 5432 # 5432 is the default port of postgress database
    to_port   = 5432
    security_groups = [ # limitate the access only to bastion server
      aws_security_group.bastion.id,
      aws_security_group.ecs_service.id # to gives access to the ECS services to the database
    ]
  }

  tags = local.common_tags
}

resource "aws_db_instance" "main" {
  identifier              = "${local.prefix}-db" # to access trhough command line
  name                    = "recipe"             # name of the main database
  allocated_storage       = 20
  storage_type            = "gp2"
  engine                  = "postgres"
  engine_version          = "11.4"
  instance_class          = "db.t2.micro"
  db_subnet_group_name    = aws_db_subnet_group.main.name
  password                = var.db_password
  username                = var.db_username
  backup_retention_period = 0
  multi_az                = false # Available in multiple zones
  skip_final_snapshot     = true  # Avoid to creates the final database snapshot, makes easy to create and recreate by TF
  vpc_security_group_ids  = [aws_security_group.rds.id]

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-main" })
  )
}
