# # creates a data record got by the DNS zone name informed into vars
# data "aws_route53_zone" "zone" {
#   name = "${var.dns_zone_name}." # this "." at the end is a DNS name convetion
# }

# # creates a new record, that creates a subdomain by env (workspace)
# # the subdomain when workspace is "dev" will look like this: api.dev.my_dns_name.com
# # lookup is a TF function that looks for a key into a map and retrieves its value
# resource "aws_route53_record" "app" {
#   zone_id = data.aws_route53_zone.zone.zone_id
#   name    = "${lookup(var.subdomain, terraform.workspace)}.${data.aws_route53_zone.zone.name}"
#   type    = "CNAME" # to forward the request to the Loab Balanacer Canonical Name
#   ttl     = "300" # time to live: when changes is made, it will take this time to be replicated

#   records = [aws_lb.api.dns_name] # name to forward the reuquests (the LB name)
# }

# resource "aws_acm_certificate" "cert" {
#   domain_name       = aws_route53_record.app.fqdn # fully qualified domain name
#   validation_method = "DNS"

#   tags = local.common_tags

#   lifecycle {
#     create_before_destroy = true
#   }
# }

# # record created to validate the certificate ownership
# resource "aws_route53_record" "cert_validation" { # the validation options are crated by the cert resource
#   name    = aws_acm_certificate.cert.domain_validation_options.0.resource_record_name
#   type    = aws_acm_certificate.cert.domain_validation_options.0.resource_record_type
#   zone_id = data.aws_route53_zone.zone.zone_id
#   records = [
#     aws_acm_certificate.cert.domain_validation_options.0.resource_record_value # randon string created to validte
#   ]
#   ttl = "60" # time to complete the validation before timeout
# }

# # used to trigger the validation resource
# # doesn't exactly creates a resource
# resource "aws_acm_certificate_validation" "cert" {
#   certificate_arn         = aws_acm_certificate.cert.arn
#   validation_record_fqdns = [aws_route53_record.cert_validation.fqdn]
# }
