resource "aws_ecs_cluster" "main" {
  name = "${local.prefix}-cluster"

  tags = local.common_tags
}

# AWS docs: https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_execution_IAM_role.html
# used to start the container
resource "aws_iam_policy" "task_execution_role_policy" {
  name        = "${local.prefix}-task-exec-role-policy"
  path        = "/"
  description = "Allow retrieving images and adding to logs"
  policy      = file("./templates/ecs/task-exec-role.json")
}

resource "aws_iam_role" "task_execution_role" {
  name               = "${local.prefix}-task-exec-role"
  assume_role_policy = file("./templates/ecs/assume-role-policy.json")
}

resource "aws_iam_role_policy_attachment" "task_execution_role" {
  role       = aws_iam_role.task_execution_role.name
  policy_arn = aws_iam_policy.task_execution_role_policy.arn
}

# used by container in runtime
# permission used after start the container
resource "aws_iam_role" "app_iam_role" {
  name               = "${local.prefix}-api-task"
  assume_role_policy = file("./templates/ecs/assume-role-policy.json")

  tags = local.common_tags
}

resource "aws_cloudwatch_log_group" "ecs_task_logs" {
  name = "${local.prefix}-api"

  tags = local.common_tags
}

# template file that will load the content of "templates/ecs/container-definitions.json.tpl" file
data "template_file" "api_container_definitions" {
  template = file("templates/ecs/container-definitions.json.tpl")

  # replace the values with ${} (terraform var sixtax)
  vars = {
    app_image         = var.ecr_image_api
    proxy_image       = var.ecr_image_proxy
    django_secret_key = var.django_secret_key
    db_host           = aws_db_instance.main.address
    db_name           = aws_db_instance.main.name
    db_user           = aws_db_instance.main.username
    db_pass           = aws_db_instance.main.password
    log_group_name    = aws_cloudwatch_log_group.ecs_task_logs.name
    log_group_region  = data.aws_region.current.name
    allowed_hosts     = aws_lb.api.dns_name # allow Load Balancer access
    # allowed_hosts = aws_route53_record.app.fqdn # allow created Route53 record domain name
    s3_storage_bucket_name   = aws_s3_bucket.app_public_files.bucket
    s3_storage_bucket_region = data.aws_region.current.name
  }
}

# definition of task that will create the containers
resource "aws_ecs_task_definition" "api" {
  family                = "${local.prefix}-api" # just like a name
  container_definitions = data.template_file.api_container_definitions.rendered
  # rended: reads the file and replace the var with setted values above
  requires_compatibilities = ["FARGATE"]
  # FARGATE: type o ECS hosting, allow to manage container without managing servers, like a serverless version of deployng containers
  network_mode       = "awsvpc"
  cpu                = 256
  memory             = 512
  execution_role_arn = aws_iam_role.task_execution_role.arn
  task_role_arn      = aws_iam_role.app_iam_role.arn

  volume {
    name = "static"
  }

  tags = local.common_tags
}

# the security group to be attached to the ECS service
resource "aws_security_group" "ecs_service" {
  description = "Access for the ECS service"
  name        = "${local.prefix}-ecs-service"
  vpc_id      = aws_vpc.main.id

  # outbound (from here to out) access to download images and any other things from internet through https
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # whole internet IPs
  }

  # out to gives access to the running services to the database only through private subnets
  egress {
    from_port = 5432
    to_port   = 5432
    protocol  = "tcp"
    cidr_blocks = [
      aws_subnet.private_a.cidr_block,
      aws_subnet.private_b.cidr_block,
    ]
  }

  # inbound (from out to here) to gives access from all internet to the running services throug port 8000
  ingress {
    from_port = 8000 # the PROXY port, the only way that is allowed to access the application
    to_port   = 8000
    protocol  = "tcp"
    #cidr_blocks = ["0.0.0.0/0"] 
    security_groups = [ # changed to allow only LB instead all internet
      aws_security_group.lb.id
    ]
  }

  tags = local.common_tags
}

# the ECS service itself
resource "aws_ecs_service" "api" {
  name             = "${local.prefix}-api"
  cluster          = aws_ecs_cluster.main.name
  task_definition  = aws_ecs_task_definition.api.family # the definition is passed by the FAMILY name
  desired_count    = 1                                  # number of running services that you need (like Kubernetes replicas)
  launch_type      = "FARGATE"
  platform_version = "1.4.0"

  # gived access through public subnets to test
  # it will be changed when we create the ELB (LoadBalancer) to receive all requests and redirect it
  network_configuration {
    subnets = [
      # aws_subnet.public_a.id, #without LB
      # aws_subnet.public_b.id,
      aws_subnet.private_a.id, # with LB only it should be exposed on public subnets
      aws_subnet.private_b.id,
    ]
    security_groups = [aws_security_group.ecs_service.id]
    #assign_public_ip = true # to allow us to access the application # with LB remove it
  }

  # add the tasks to the LB target group
  load_balancer {
    target_group_arn = aws_lb_target_group.api.arn
    container_name   = "proxy"
    container_port   = 8000
  }

  # # used with Certificate
  # depends_on = [aws_lb_listener.api_https] # manually inform a dependency to tells Terraform the correct order to create resourses
}

data "template_file" "ecs_s3_write_policy" {
  template = file("templates/ecs/s3-write-policy.json.tpl") # policy to allow app to red/write into s3 bucket

  vars = {
    bucket_arn = aws_s3_bucket.app_public_files.arn # change var dinamically
  }
}

# create policy with the content of the rendered template file
resource "aws_iam_policy" "ecs_s3_access" {
  name        = "${local.prefix}-AppS3AccessPolicy"
  path        = "/"
  description = "Allow access to the recipe app S3 bucket"

  policy = data.template_file.ecs_s3_write_policy.rendered
}

# attach the created policy to the role created before and attatched to the Task Definition,
# that will give access to the containers into bucket
resource "aws_iam_role_policy_attachment" "ecs_s3_access" {
  role       = aws_iam_role.app_iam_role.name
  policy_arn = aws_iam_policy.ecs_s3_access.arn
}
