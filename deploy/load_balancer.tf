resource "aws_lb" "api" {
  name               = "${local.prefix}-main"
  load_balancer_type = "application"
  subnets = [
    aws_subnet.public_a.id,
    aws_subnet.public_b.id
  ]

  security_groups = [aws_security_group.lb.id]

  tags = local.common_tags
}

# The gout that the LB can forward requests to
# ECS tasks will be added to it in order to be served by LB
resource "aws_lb_target_group" "api" {
  name        = "${local.prefix}-api"
  protocol    = "HTTP" # LB will user HTTP to request our API
  vpc_id      = aws_vpc.main.id
  target_type = "ip" # using ip address to add into LB
  port        = 8000 # port where our PROXY is running on ECS task

  #LB feature that allow it to perform reqs to ensure that app is running
  # if not healthy, LB tells to the ECS to restart the app
  health_check {
    path = "/admin/login/" # app route
  }
}

# entry point of LB
resource "aws_lb_listener" "api" {
  load_balancer_arn = aws_lb.api.arn
  port              = 80
  protocol          = "HTTP"

  # receives request into http port 80 and forward to the target group 
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.api.arn
  }
}

# allow access into the LB
resource "aws_security_group" "lb" {
  description = "Allow access to Application Load Balancer"
  name        = "${local.prefix}-lb"
  vpc_id      = aws_vpc.main.id

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  # # used when configured DNS and Certificate
  # ingress {
  #   protocol    = "tcp"
  #   from_port   = 443
  #   to_port     = 443
  #   cidr_blocks = ["0.0.0.0/0"]
  # }

  egress {
    protocol    = "tcp"
    from_port   = 8000
    to_port     = 8000
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = local.common_tags
}

# # use this HTTP listener when activate custom DNS and Certificate
# resource "aws_lb_listener" "api" {
#   load_balancer_arn = aws_lb.api.arn
#   port              = 80
#   protocol          = "HTTP"

#   default_action {
#     type = "redirect"

#     redirect { # redirect from HTTP to HTTPS instead the ECS tasks
#       port        = "443"
#       protocol    = "HTTPS"
#       status_code = "HTTP_301" # default HTTP redirect response
#     }
#   }
# }

# # LB listener to handle HTTPS requests
# resource "aws_lb_listener" "api_https" {
#   load_balancer_arn = aws_lb.api.arn
#   port              = 443
#   protocol          = "HTTPS"

#   certificate_arn = aws_acm_certificate_validation.cert.certificate_arn # used cert validation to guarantee that the validation will run before create this listener

#   default_action {
#     type             = "forward"
#     target_group_arn = aws_lb_target_group.api.arn
#   }
# }
