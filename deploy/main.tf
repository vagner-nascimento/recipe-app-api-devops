terraform {
  backend "s3" {
    bucket         = "recipe-app-api-devops-tfstate-vn"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }

}

provider "aws" {
  region  = "us-east-1"
  version = "~> 3.0"
}

# define locals variables
locals {
  # "var.prefix" = taking the default value of the "prefix" var setted on variables tf file
  # "terraform.workspace" is the crrent terrform workspace
  # "${}" is the interpolation terraform syntax, that allow us to dynamic create variables
  prefix = "${var.prefix}-${terraform.workspace}"

  # create common tags to put on every created resource
  # tags make easy to find a specific resource when need to debug something on the cloud (which can contain thousen of resources)
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

# Used to retrive the current region where TF will be applied / avoid to hard code region
data "aws_region" "current" {}
