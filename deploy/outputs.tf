# TF outputs are used to print info about the created resources
# This will print the address of the created RDS database to allow to access it
output "db_host" {
  value = aws_db_instance.main.address
}

# the oublic hostname to access from internet
output "bastion_host" {
  value = aws_instance.bastion.public_dns
}

# LB url
output "api_endpoint" {
  value = aws_lb.api.dns_name
  #  value = aws_route53_record.app.fqdn # used with Custom DNS and Ceritificate
}
