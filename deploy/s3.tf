resource "aws_s3_bucket" "app_public_files" {
  bucket        = "${local.prefix}-files"
  acl           = "public-read"
  force_destroy = true # set to true to allow T erraform to destroy it. the default is false by safety
}
