# create variables to share between the tf files
# "default" is the the default value of it (can be changed passed through terraform commands or files)
# ANY variable can be changed by CI/CD vars when run the pipeline
variable "prefix" {
  type        = string
  default     = "raad"
  description = "App prefix"
}

# add variables to be used as tag on the aws resources
variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "vagnernasc88@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

# name of ssh pub key added on aws EC2 console
variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

# default image used when run local (out of pipeline)
variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "417662913057.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for Proxy"
  default     = "417662913057.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

# without default for security reasons
variable "django_secret_key" {
  description = "Secret key for Django app"
}

# Custom DNS
variable "dns_zone_name" {
  description = "Domain name"
  default     = "not_created.not" # the DNS created on AWS console
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string) # creates a map (key:val object)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}
