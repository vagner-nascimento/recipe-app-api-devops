#!/bin/sh

set -e

python manage.py collectstatic --noinput # Django command to collect the static files to send to the nginx proxy server
python manage.py wait_for_db
python manage.py migrate # Django command to run database migration (database exchanges)

uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi
